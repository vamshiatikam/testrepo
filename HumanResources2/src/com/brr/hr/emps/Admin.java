package com.brr.hr.emps;

import com.brr.hr.BasicEmployee;

public class Admin extends BasicEmployee {
	
	private int landline;

	public Admin(int id, int landline) {
		super(id);
		this.landline = landline;
	}

	public int getLandline() {
		return landline;
	}

	public void setLandline(int landline) {
		this.landline = landline;
	}
	
	

}
