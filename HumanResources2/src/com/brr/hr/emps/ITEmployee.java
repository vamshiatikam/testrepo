package com.brr.hr.emps;

import com.brr.hr.BasicEmployee;

public class ITEmployee extends BasicEmployee {
	
	String project;
	String team;
	String deadline;
	
	
	public ITEmployee(int id, String project) {
		super(id);
		this.project = project;
	}


	public String getProject() {
		return project;
	}


	public void setProject(String project) {
		this.project = project;
	}


	public String getTeam() {
		return team;
	}


	public void setTeam(String team) {
		this.team = team;
	}


	public String getDeadline() {
		return deadline;
	}


	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}
	
	
	
}