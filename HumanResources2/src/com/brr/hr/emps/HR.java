package com.brr.hr.emps;

import com.brr.hr.BasicEmployee;

public class HR extends BasicEmployee {
 private int landline;

public HR(int id, int landline) {
	super(id);
	this.landline = landline;
}

public int getLandline() {
	return landline;
}

public void setLandline(int landline) {
	this.landline = landline;
}
 

}
