package com.brr.hr;

public class BasicEmployee {

	
	private int id;
	private String name;
	private String company;
	private String dept;
	private float sal;
	private int mobile;
	private String manager;
	
	
	public BasicEmployee(int id) {
		super();
		this.id = id;
	}


	public BasicEmployee() {
		// TODO Auto-generated constructor stub
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getDept() {
		return dept;
	}


	public void setDept(String dept) {
		this.dept = dept;
	}


	public float getSal() {
		return sal;
	}


	public void setSal(float sal) {
		this.sal = sal;
	}


	public int getMobile() {
		return mobile;
	}


	public void setMobile(int mobile) {
		this.mobile = mobile;
	}


	public String getManager() {
		return manager;
	}


	public void setManager(String manager) {
		this.manager = manager;
	}
	
	public void BasicEmployee(int id,String name, float sal) {
		this.id= id;
		this.name = name;
		this.sal = sal;
		this.company = company;
		this.dept = dept;
	}
	
	
	public void print() {
		System.out.println(this.id + " " + this.name +" "+ this.sal + " " + this.company + " " + this.dept);
	}
	
	
	
	
	
}
